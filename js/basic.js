function cerrar(element) {
    let elemento = element.parentElement.parentElement.parentElement
    elemento.classList.remove("d-flex");
    elemento.classList.add("d-none");
}

function verModalLink() {
    let modal = document.getElementById('modal-link')
    modal.classList.add("d-flex");
}

function verModalEliminar() {
    let modal = document.getElementById('modal-eliminar')
    modal.classList.add("d-flex");
}

function abrirNotificaciones(element) {
    let opciones = element.parentElement.children[2]
    let alto = element.parentElement.children[2].children.length * 45

    if (element.parentElement.children[2].clientHeight === 0) {
        opciones.style.height = alto + "px"
    } else {
        opciones.removeAttribute('style')
    }
}